<header>
PROXIMA PHOTO APP
</header>
Learn how this is work, this is just like a social apps

# FEATURE OF PROXIMA
 Add friend and making social with them
 Sharing your photo with privacy
 Like and comment in the feed
 Share and find the happiness

# BUGs ON THIS SOURCE
 App still have bug when FOLLOW button pressed
 still finding the bugs, if you found one, please tell me, or email to heri.setiawan@dkatalis.com

# DEPENDENCIES
 NODE

# HOW TO RUN THIS APP???
 * Clone this repository
 * You will get 2 folder, Proxima and Proxima-server
 * First make sure your PC and Phone connected with same network, such like a WiFi
 * Ckeck IPv4 Address that used by PC
 * After that, open /Proxima/config/index.js and change host with IPv4 you get
 * Run command 'npm i' in each of folder
 * After that, all think ready, you can run this app with:
   * **npx react-native start** and **npx react-native run-android** in folder Proxima
   * **nodemon** in folder Proxima-Server

# HOW TO RUN THE TEST FILE

 I've not creating test file, maybe you can help me to create it