const { User } = require('../models/user')
const { Feed } = require('../models/feed')
const md5 = require('md5')
const btoa = require('btoa')
const { checkEmail, checkUsername } = require('../validators/checker')
const { email, password } = require('../validators/index')
const path = require('path')
const fs = require('fs')

exports.showFeed = async (request, h) => {
    const { email, password } = request.payload
    const emailFind = await checkEmail(email)
    if (emailFind[0] !== undefined) {
        if (password === emailFind[0].password) {
            const showOnly = [].concat(email, emailFind[0].following)
            const feed = await Feed.find({ owner: { '$in': showOnly } }).sort({ updatedAt: -1 })
            h.response().code(200)
            return feed
        } else {
            return h.response([{ auth: false, message: 'Invalid email or password' }]).code(401)
        }
    } else {
        return h.response([{ auth: false, message: 'Email not registered' }]).code(400)
    }
}

exports.newFeed = async (request, h) => {
    const { email, password, caption } = request.headers
    const emailFind = await checkEmail(email)
    if (emailFind[0] !== undefined) {
        if (password === emailFind[0].password) {
            const feedId = Math.floor(Math.random() * 10 ** 10)
            let extension = path.extname(request.payload.picture.hapi.filename)
            let random = Math.floor(Math.random() * 10 ** 10)
            let fileName = email + "-" + random + extension;
            (request.payload.picture).pipe(fs.createWriteStream(__dirname + "/../src/post/" + fileName))
            const newFeed = new Feed({
                feedId: feedId,
                owner: email,
                caption: caption,
                createdAt: new Date().toISOString(),
                updatedAt: new Date().toISOString(),
                imgUrl: fileName

            })
            await newFeed.save()
            return h.response([{ auth: true, message: 'Post new feed success', feedId: feedId }]).code(201)
        } else {
            return h.response([{ auth: false, message: 'Invalid email or password' }]).code(401)
        }
    } else {
        return h.response([{ auth: false, message: 'Email not registered' }]).code(400)
    }
}

exports.addComment = async (request, h) => {
    const { email, password, feedId, comment } = request.payload
    const emailFind = await checkEmail(email)
    if (emailFind[0] !== undefined) {
        if (password === emailFind[0].password) {
            let getComment = (await Feed.find({ feedId: feedId }))[0].comments
            getComment = getComment ? getComment : []
            const newComment = {
                date: new Date(),
                owner: email,
                comment: comment
            }
            const setComment = getComment.concat(newComment)
            return await Feed.updateOne({ feedId: feedId }, { comments: setComment, updatedAt: new Date().toISOString() }, { new: true })
        } else {
            return h.response([{ auth: false, message: 'Invalid email or password' }]).code(401)
        }
    } else {
        return h.response([{ auth: false, message: 'Email not registered' }]).code(400)
    }
}

exports.addLike = async (request, h) => {
    const { email, password, feedId } = request.payload
    const emailFind = await checkEmail(email)
    if (emailFind[0] !== undefined) {
        if (password === emailFind[0].password) {
            let getLike = (await Feed.find({ feedId: feedId }))[0].likes
            if (!getLike.includes(email)) {
                getLike = getLike ? getLike : []
                const setLike = getLike.concat(email)
                return await Feed.updateOne({ feedId: feedId }, { likes: setLike, updatedAt: new Date().toISOString() }, { new: true })
            } return h.response({ message: 'You have been liked this feed' }).code(200)
        } else {
            return h.response([{ auth: false, message: 'Invalid email or password' }]).code(401)
        }
    } else {
        return h.response([{ auth: false, message: 'Email not registered' }]).code(400)
    }
}