const { User } = require('../models/user')
const { Feed } = require('../models/feed')
const md5 = require('md5')
const btoa = require('btoa')
const { checkEmail, checkUsername } = require('../validators/checker')
const { email, password } = require('../validators/index')
const path = require('path')
const fs = require('fs')

exports.updateInfo = async (request, h) => {
    const { username } = request.payload
    const { email, password } = request.headers
    const emailFind = await checkEmail(email)
    if (emailFind[0] !== undefined) {
        if (password === emailFind[0].password) {
            const usernameFind = await User.find({ username: username })[0]
            if (usernameFind === undefined || usernameFind.email == email) {
                await User.updateOne({ email: email }, request.payload)
                h.response().code(202)
                return User.find({ email: email })
            } else {
                return h.response([{ auth: false, message: 'Username already used' }]).code(409)
            }
        } else {
            return h.response([{ auth: false, message: 'You haven\'t access here' }]).code(401)
        }
    } else {
        return h.response([{ auth: false, message: 'Email not registered' }]).code(400)
    }
}

exports.setupDisplayPicture = async (request, h) => {
    const { email, password } = request.headers
    const emailFind = await checkEmail(email)
    if (emailFind[0] !== undefined) {
        if (password === emailFind[0].password) {
            let extension = path.extname(request.payload.profile_picture.hapi.filename)
            let random = Math.floor(Math.random() * 10 ** 10)
            let fileName = email + "-" + random + extension;
            (request.payload.profile_picture).pipe(fs.createWriteStream(__dirname + "/../src/profileImg/" + fileName))
            await User.updateOne({ email: email }, { profilePic: fileName })
            return h.response({ message: "File uploaded", file: fileName });
        } else {
            return h.response([{ auth: false, message: 'You haven\'t access here' }]).code(401)
        }
    } else {
        return h.response([{ auth: false, message: 'Email not registered' }]).code(400)
    }
}

exports.userCheck = async (request, h) => {
    const { email, password } = request.payload
    const emailFind = await checkEmail(email)
    if (emailFind[0] !== undefined) {
        if (md5(password) === emailFind[0].password) {
            return h.response([{ auth: true }]).code(200)
        } else {
            return h.response([{ auth: false, message: 'Invalid email or password' }]).code(401)
        }
    } else {
        return h.response([{ auth: false, message: 'Email not registered' }]).code(400)
    }
}

exports.findUser = async (request, h) => {
    const { contain } = request.query
    return await User.find({}, { _id: 0, birth: 0, birth: 0, password: 0, id: 0, __v: 0 }).sort({ firstName: 1 })
}

exports.countFeedFollow = async (request, h) => {
    const feedCount = await Feed.find({ owner: request.payload.email })
    const following = (await User.find({ email: request.payload.email }))[0].following
    const followers = (await User.find({ "following": { "$regex": request.payload.email, "$options": "i" } })).map(x => x.email)
    return h.response({ feedCount, following, followers }).code(200)
}

exports.following = async (request, h) => {
    const { email, password, target } = request.payload
    // console.log(email, password, target)
    const emailFind = await checkEmail(email)
    if (emailFind[0] !== undefined) {
        if (password === emailFind[0].password) {
            const getFollow = (await User.find({ email: request.payload.email }))[0].following
            if (!getFollow.includes(email)) {
                const setFollow = getFollow.concat(target)
                return await User.updateOne({ email: email }, { following: setFollow })
            } return h.response({ message: 'You have been followed this user' }).code(200)
        } else {
            return h.response([{ auth: false, message: 'Invalid email or password' }]).code(401)
        }
    } else {
        return h.response([{ auth: false, message: 'Email not registered' }]).code(400)
    }
}
// For show who following us
// db.users.find({"following": { "$regex": "heri@realuniversity.ac.id", "$options": "i" }}).pretty()