const email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@realuniversity\.ac\.id$/
const password = /^[a-zA-Z)-9]{6,}$/

module.exports = { email, password }