const { updateInfo, setupDisplayPicture, userCheck, findUser, countFeedFollow, following } = require('../controllers/user')

module.exports = [
    {
        method: 'PUT',
        path: '/profile',
        handler: updateInfo
    },
    {
        method: 'POST',
        path: '/profile/check',
        handler: userCheck
    },
    {
        method: 'PUT',
        path: '/profile/picture',
        config: {
            payload: {
                output: "stream",
                parse: true,
                maxBytes: 2 * 1000 * 1000
            }
        },
        handler: setupDisplayPicture
    },
    {
        method: 'GET',
        path: '/image/{type}/{picname}',
        handler: (request, h) => {
            return h.file(`${request.params.type}/${request.params.picname}`)
        },
        options: {
            auth: false
        }
    },
    {
        method: 'GET',
        path: '/find',
        handler: findUser
    },
    {
        method: 'POST',
        path: '/getfollow',
        handler: countFeedFollow
    },
    {
        method: 'POST',
        path: '/follow',
        handler: following
    }
]