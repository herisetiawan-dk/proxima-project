const { showFeed, newFeed, addLike, addComment } = require('../controllers/feed')

module.exports = [
    {
        method: 'POST',
        path: '/feed',
        handler: showFeed
    },
    {
        method: 'PUT',
        path: '/feed',
        handler: newFeed,
        config: {
            payload: {
                output: "stream",
                parse: true,
                maxBytes: 2 * 1000 * 1000
            }
        },
    },
    {
        method: 'POST',
        path: '/like',
        handler: addLike
    },
    {
        method: 'POST',
        path: '/comment',
        handler: addComment
    }
]