const auth = require('./auth')
const user = require('./user')
const feed = require('./feed')

module.exports = [].concat(auth, user, feed)