const mongoose = require('mongoose')
const database = 'mongodb://localhost:27017/proxima-photoapp'

const mongodb = async () => {
    try {
        await mongoose.connect(database, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        })
        console.log(database)
    } catch (err) {
        console.error(err.message)
        process.exit(1)
    }
}

module.exports = { mongodb }