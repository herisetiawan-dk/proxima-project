const mongoose = require('mongoose')
const { Schema } = mongoose

const userModel = new Schema({
    id: Number,
    firstName: String,
    lastName: String,
    username: String,
    email: String,
    birth: Date,
    password: String,
    profilePic: String,
    following: Array
})

const User = mongoose.model('user', userModel)

module.exports = { User }