import React, { Component } from 'react'
import { StatusBar } from 'react-native'
import { Provider } from 'react-redux'
import Navigator from './routes/index'
import store from './store'
import { mainColor, secColor, thirdColor } from './styles/colors'

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <StatusBar backgroundColor={thirdColor} />
                <Navigator />
            </Provider>
        )
    }
}