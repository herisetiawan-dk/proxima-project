const email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@realuniversity\.ac\.id$/
const password = /^[a-zA-Z0-9]{6,}$/
const name = /^[A-Z]{1}[a-z]{0,19}$/
const username = /^[a-zA-Z0-9]{1,20}$/

module.exports = { email, password, name, username }