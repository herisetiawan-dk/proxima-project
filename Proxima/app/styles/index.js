import { StyleSheet } from 'react-native'
import { mainColor, secColor, thirdColor, forthColor } from './colors'

const style = StyleSheet.create({
    fullContainer: {
        width: '100%',
        height: '100%'
    },
    container: {
        backgroundColor: mainColor,
        justifyContent: 'center'
    },
    titleHeader: {
        color: secColor,
        fontWeight: 'bold',
        fontSize: 40,
        textAlign: 'center'
    },
    formContainer: {
        alignSelf: 'center',
        width: '80%'
    },
    basicInput: {
        textAlign: 'center',
        marginTop: '5%',
        color: secColor
    },
    buttonMargin: {
        margin: '5%'
    },
    pressButton: {
        alignSelf: 'center',
        backgroundColor: secColor,
        borderRadius: 20,
        width: '70%',
        height: 40,
        justifyContent: 'center',
    },
    pressButtonTxt: {
        color: mainColor,
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 20
    },
    litleDesc: {
        marginHorizontal: '15%',
        color: '#aaa',
        textAlign: 'justify'
    },
    footer: {
        margin: '15%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    basicButton: {
        borderRadius: 20,
        height: 40,
        width: '40%',
        // borderWidth: 5,
    },
    landRegisterButton: {
        justifyContent: 'center',
        backgroundColor: secColor

    },
    landRegisterTitle: {
        color: mainColor,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    landLoginButton: {
        justifyContent: 'center',
        backgroundColor: mainColor
    },
    landLoginTitle: {
        color: secColor,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    displayPicture: {
        height: 200,
        width: 200,
        borderRadius: 100,
        alignSelf: 'center',
        borderWidth: 5,
        borderColor: thirdColor
    },
    bottomWarning: {
        color: secColor,
        fontSize: 10
    },
    disabled: {
        opacity: 0.5
    },
    emptyFeed: {
        backgroundColor: secColor,
        width: '95%',
        height: '85%',
        alignSelf: 'center',
        borderRadius: 30,
        justifyContent: 'center',
        paddingHorizontal: '20%'
    },
    emptyFeedIcon: {
        alignSelf: 'center',
        color: forthColor
    },
    emptyFeedTxt: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 15,
        color: forthColor
    },
    feedContainer: {
        marginBottom: 10,
        backgroundColor: secColor,
        width: '95%',
        alignSelf: 'center',
        borderRadius: 10,
        padding: 20,
        paddingTop: 10
    },
    feedImage: {
        borderRadius: 5,
        height: 200,
        width: '100%',
    },
    headerContainer: {
        backgroundColor: secColor,
        height: 60,
        width: '95%',
        alignSelf: 'center',
        borderRadius: 30,
        marginBottom: 5,
        flexDirection: 'row'
    },
    plusButton: {
        backgroundColor: mainColor,
        justifyContent: 'center',
        width: 60,
        height: 60,
        borderRadius: 30,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        position: 'absolute',
        bottom: 10,
        right: 10,
    },
    plusButtonIcon: {
        color: secColor,
        alignSelf: 'center',
    },
    titleHeaderAlt: {
        color: 'black'
    },
    secContainer: {
        padding: 10,
        backgroundColor: secColor,
        width: '95%',
        height: '95%',
        alignSelf: 'center',
        borderRadius: 10
    },
    captionInput: {
        color: mainColor,
        borderWidth: 2,
        borderColor: mainColor,
        borderRadius: 10,
        maxHeight: 100
    },
    pressButtonAlt: {
        backgroundColor: mainColor,
        marginTop: 10
    },
    pressButtonTxtAlt: {
        color: secColor
    },
    elevate: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
    },
    moveToBottom: {
        position: 'absolute',
        bottom: 50,
    },
    feedOwner: {
        fontWeight: 'bold',
        // backgroundColor: sec,
        
        paddingBottom: 10,
        fontSize: 16,
        // borderBottomWidth: 1,
        // borderTopWidth: 1,
        // borderColor: mainColor,
        // marginBottom: '3%'
    },
    findContainer: {
        flexDirection: 'row',
        marginVertical: 5,
        backgroundColor: mainColor,
        borderRadius: 10,
        padding: 10
    },
    mergeFirstLastName: {
        flexDirection: 'row'
    },
    nameStyle: {
        color: secColor,
        fontWeight: 'bold'
    },
    usernameStyle: {
        color: secColor,
        opacity: 0.8
    },
    profilePic: {
        height: '50%',
        width: '90%',
        borderRadius: 200,
        marginTop: 10,
        alignSelf: 'center'
    },
    miniBoxContainer: {
        flexDirection: 'row',
        justifyContent: "space-between",
        paddingHorizontal: '5%'
    },
    miniBoxContainerSub: {
        borderBottomWidth: 3,
        width: '25%',
        borderColor: mainColor
    },
    miniBoxNumber: {
        textAlign: 'center',
        color: mainColor,
        fontWeight: 'bold',
        fontSize: 30
    },
    miniBoxTitle: {
        textAlign: 'center'
    },
    emailProfile: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 15,
        fontStyle: 'italic'
    },
    usernameProfile: {
        textAlign: 'center'
    },
    findDP: { width: 40, height: 40, borderRadius: 20 },
    likeComment: {
        flexDirection: 'row',
        marginTop: 5,
    },
    likeCommentTxt: {
        fontWeight: 'bold',
        marginRight: 10,
        marginLeft: 5,
        fontSize: 13
    },
    likeCount: {
        fontWeight: 'bold',
        fontSize: 10,
        marginBottom: 5
    },
    commentContainer: {
        flexDirection: 'row'
    },
    commentInput: {
        flex: 1,
        height: 35,
        fontSize: 12,
        borderWidth: 0.5,
        borderRadius: 5,
        marginRight: 10
    },
    commentDisabled: {
        opacity: 0.50
    },
    commentList: {
        flexDirection: 'row'
    },
    commentTxtBasic: {
        fontSize: 12,
        textAlign: 'justify',
        flexWrap: 'wrap'
    },
    commentOwner: {
        fontWeight: 'bold'
    },
    textInputProfile: {
        marginHorizontal: '10%',
        marginTop: 0,
        marginBottom: 0,
        color: mainColor
    }
})

export default style