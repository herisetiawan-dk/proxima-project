import React, { Component } from 'react'
import { NavigationEvents } from 'react-navigation'
import { View, Text, ActivityIndicator, FlatList } from 'react-native'
import style from '../styles'
import { mainColor, secColor, thirdColor, forthColor } from '../styles/colors'

export default class ListUser extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            listUser: ''
        }
    }

    refreshList = () => {
        this.setState({
            listUser: this.props.navigation.state.params
        })
    }

    clearList = () => {
        this.setState({
            listUser: ''
        })
    }

    renderUser = (user) => {
        return (
            <View style={[style.secContainer, style.elevate, { height: 40, marginVertical: 5 }]}>
                <Text>
                    {user}
                </Text>
            </View>
        )
    }

    render() {
        console.log(this.state.listUser)
        if (this.state.listUser == '') {
            return (
                <View style={[style.fullContainer, style.container]}>
                    <NavigationEvents
                        onDidFocus={() => this.refreshList()}
                        onDidBlur={() => this.clearList()}
                    />
                    <ActivityIndicator color={secColor} size='large' />
                </View>
            )
        }
        return (
            <View style={[style.container, style.fullContainer]}>
                <NavigationEvents
                    onDidFocus={() => this.refreshList()}
                    onDidBlur={() => this.clearList()}
                />
                <View style={[style.secContainer]}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.listUser}
                        renderItem={(user) => this.renderUser(user.item)}
                    />
                </View>
            </View>
        )
    }
}