import React, { Component } from 'react'
import { Alert, BackHandler, SectionList, TextInput, FlatList, View, Text, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import { NavigationEvents } from 'react-navigation'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage'
import Icon from 'react-native-vector-icons/AntDesign'
import ImagePicker from 'react-native-image-picker'
import style from '../styles'
import { imageSet, imageRemove } from '../action/feed'
import { mainColor, secColor, thirdColor, forthColor } from '../styles/colors'
import { getFeed, likeFeed, commentFeed } from '../action/feed'
import { server } from '../config'

class Feed extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            user: {},
            feedId: '',
            comment: ''
        }
    }

    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        const user = JSON.parse(await AsyncStorage.getItem('USER'))
        this.setState({
            user
        })
        await AsyncStorage.setItem('LastPlace', 'Feed')
        this.getFeed(true)
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        BackHandler.exitApp()
        return true;
    }

    getFeed(refresh) {
        this.props.getFeed(refresh)
    }

    likeFeed = (feedId) => {
        this.props.likeFeed(feedId)
        this.getFeed(false)
    }

    addComment = (feedId) => {
        if (feedId == this.state.feedId) {
            this.setState({ feedId: '' })
        } else {
            this.setState({ feedId: feedId })
        }
    }

    renderComment = (comment) => {
        return (
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={comment}
                renderItem={(comment) => this.commentList(comment.item)}
            />

        )
    }

    commentList = (comment) => {
        return (
            <View style={style.commentList}>
                <Text style={style.commentTxtBasic}>
                    <Text style={style.commentOwner}>
                        {comment.owner}
                    </Text>:
                        {comment.comment}
                </Text>
            </View>
        )
    }

    sendComment = (feedId) => {
        this.props.commentFeed(feedId, this.state.comment)
        this.setState({
            comment: ''
        }),
            this.getFeed(false)
    }

    renderFeed = (feed) => {
        return (
            <View style={[style.feedContainer, style.elevate]}>
                <Text style={[style.feedOwner, style.elevate]}>
                    {feed.owner}
                </Text>
                <Image source={{ uri: `${server}/image/post/${feed.imgUrl}` }} style={style.feedImage} />
                <View style={[style.likeComment, { justifyContent: 'space-between' }]}>
                    <View style={style.likeComment}>
                        <View style={style.commentButtonContainer}>
                            <TouchableOpacity onPress={() => this.likeFeed(feed.feedId)} disabled={(feed.likes).includes(this.state.user.email)}>
                                {(feed.likes).includes(this.state.user.email) ?
                                    <Icon name='like1' size={20} /> :
                                    <Icon name='like2' size={20} />
                                }
                            </TouchableOpacity>
                        </View>
                        <View style={[style.likeComment, { flexDirection: 'column', justifyContent: 'center' }]}>
                            <Text style={style.likeCommentTxt}>Like</Text>
                        </View>
                        <TouchableOpacity onPress={() => this.addComment(feed.feedId)}>
                            <Icon name='message1' size={20} />
                        </TouchableOpacity>
                        <View style={[style.likeComment, { flexDirection: 'column', justifyContent: 'center' }]}>
                            <Text style={style.likeCommentTxt}>Comment</Text>
                        </View>
                    </View>
                    <View style={style.likeComment}>
                        {feed.owner == this.state.user.email ? <Icon name='edit' size={20} /> : null}
                        <View style={[style.likeComment, { flexDirection: 'column', justifyContent: 'center' }]}>
                        </View>
                    </View>
                </View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('ListUser', feed.likes)}>
                    <Text style={style.likeCount}>
                        {feed.likes ? ((feed.likes).includes(this.state.user.email) ? `You and ${feed.likes.length - 1} other` : feed.likes.length) : 0} {(feed.likes).includes(this.state.user.email) ? `likes` : `Likes`} this feed
                </Text>
                </TouchableOpacity>
                <Text>
                    {feed.caption}
                </Text>
                {
                    this.state.feedId == feed.feedId &&
                    <View style={style.commentContainer}>
                        <TextInput
                            style={style.commentInput}
                            placeholder={`Comment as ${this.state.user.email}`}
                            onChangeText={(comment) => this.setState({ comment })}
                            value={this.state.comment}
                        />
                        <TouchableOpacity disabled={!this.state.comment.length > 0} onPress={() => this.sendComment(feed.feedId)}>
                            <Text style={!this.state.comment.length > 0 && style.commentDisabled}>Send</Text>
                        </TouchableOpacity>
                    </View>
                }
                {
                    feed.comments[0] !== undefined &&
                    this.renderComment(feed.comments)
                }
            </View >
        )
    }

    imagePicker = () => {
        const camOptions = {
            title: 'Change Picture',
            maxWidth: 1000,
            maxHeight: 1000,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(camOptions, (response) => {
            if (response.didCancel) {
                this.props.navigation.goBack()
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                const source = { uri: response.uri };
                this.props.imageSet(source)
                this.props.navigation.navigate('NewFeed')
            }
        });
    }

    feedEmpty = () => {
        if (this.props.feed.feed !== undefined) {
            if (this.props.feed.feed[0] !== undefined) {
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }

    render() {
        if (this.props.feed.loading) {
            return (
                <View style={[style.fullContainer, style.container]}>
                    <ActivityIndicator color={secColor} size='large' />
                </View>
            )
        }

        if (this.props.feed.err) {
            Alert.alert(
                'Failed',
                this.props.feed.err, [{
                    text: 'Try Again',
                    onPress: () => this.getFeed(true),
                    style: 'cancel'
                }, {
                    text: 'Exit',
                    onPress: () => BackHandler.exitApp()
                },], {
                cancelable: false
            }
            )
        }

        return (
            <View style={[style.fullContainer, style.container]}>
                <NavigationEvents
                    onWillFocus={() => BackHandler.addEventListener('hardwareBackPress', this.handleBackButton)}
                    onDidBlur={() => BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)}
                />
                {this.feedEmpty() ?
                    <View style={style.emptyFeed}>
                        <Icon name='camerao' size={100} style={style.emptyFeedIcon} />
                        <Text style={style.emptyFeedTxt}>
                            No feed found, Try to upload image or adding friends
                        </Text>
                    </View> :
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={(this.props.feed.feed)}
                        renderItem={(feed) => this.renderFeed(feed.item)}
                    />
                }
                <View style={style.plusButton}>
                    <TouchableOpacity onPress={this.imagePicker}>
                        <Icon name='plus' style={style.plusButtonIcon} size={40} />
                    </TouchableOpacity>
                </View>
            </View >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        feed: state.feed
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getFeed: (email, password) => {
            dispatch(getFeed(email, password))
        },
        imageSet: (source) => {
            dispatch(imageSet(source))
        },
        likeFeed: (feedId) => {
            dispatch(likeFeed(feedId))
        },
        commentFeed: (feedId, comment) => {
            dispatch(commentFeed(feedId, comment))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Feed)