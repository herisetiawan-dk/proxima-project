import React, { Component } from 'react'
import { ToastAndroid, Alert, TouchableOpacity, ActivityIndicator, Image, View, Text, TextInput, ScrollView } from 'react-native'
import style from '../styles'
import { mainColor, secColor, thirdColor, forthColor } from '../styles/colors'
import { connect } from 'react-redux'
import { userUpdate } from '../action/auth'
import { uploadDisplayPicture } from '../action/user'
import { server } from '../config'
import DateTimePicker from '@react-native-community/datetimepicker'
import ImagePicker from 'react-native-image-picker'
import AsyncStorage from '@react-native-community/async-storage'

class UserEditProfile extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        const self = this.props.navigation.state.params
        this.setState(self)
        this.setState({ birth: self.birth.slice(0, 10) })
    }

    setDate = (event, date) => {
        date = date || this.state.date
        this.setState({
            datePicker: false,
            birth: date.toISOString().slice(0, 10)
        })
    }

    imagePicker = () => {
        const camOptions = {
            title: 'Change Picture',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(camOptions, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                const source = { uri: response.uri };
                this.setState({
                    avatarSource: source,
                });
            }
        });
    }

    submitButton = () => {
        if (this.state.avatarSource) {
            this.props.uploadDisplayPicture(this.state.avatarSource)
        }
        const { firstName, lastName, birth, username } = this.state
        this.props.userUpdate(firstName, lastName, username, birth)
        this.setState({
            buttonPressed: true
        })
    }


    render() {
        if (!this.state) {
            return (
                <View style={[style.fullContainer, style.container]}>
                    <ActivityIndicator color={secColor} size='large' />
                </View>
            )
        }
        if (this.props.user.loading) {
            return (
                <View style={[style.fullContainer, style.container]}>
                    <ActivityIndicator color={secColor} size='large' />
                </View>
            )

        } else {
            if (this.state.buttonPressed) {
                if (this.props.user.auth) {
                    if (this.props.user.payload[0]) {
                        console.log(this.props.user.payload[0])
                        ToastAndroid.show('Profile updated sucessfuly', ToastAndroid.SHORT)
                        AsyncStorage.setItem('USER', JSON.stringify(this.props.user.payload[0]))
                            .then(() => this.props.navigation.navigate('Profile'))
                    }
                } else {
                    Alert.alert('Failed', this.props.user.err)
                    this.setState({
                        buttonPressed: false
                    })
                }
            }
        }
        return (
            <View style={[style.container, style.fullContainer]}>
                <View style={style.secContainer}>
                    <Text style={[style.titleHeader, style.titleHeaderAlt]}>
                        Edit Profile
                </Text>
                    <TouchableOpacity onPress={this.imagePicker}>
                        <Image source={
                            this.state.avatarSource ? this.state.avatarSource :
                                (this.state.profilePic ?
                                    { uri: `${server}/image/profileImg/${this.state.profilePic}` } :
                                    require('../src/img/default-profile-picture.jpg'))
                        } style={[style.profilePic, { width: 250, height: 250, marginBottom: 10 }]}
                        />
                    </TouchableOpacity>
                    <ScrollView>
                        <TextInput
                            value={this.state.firstName}
                            onChangeText={(firstName => this.setState({ firstName }))}
                            style={[style.basicInput, style.textInputProfile]}
                            underlineColorAndroid={mainColor}
                            placeholder='First Name'
                            returnKeyType='next'
                            onSubmitEditing={() => this.lastNameType.focus()}
                        />
                        <TextInput
                            value={this.state.lastName}
                            onChangeText={(lastName => this.setState({ lastName }))}
                            style={[style.basicInput, style.textInputProfile]}
                            underlineColorAndroid={mainColor}
                            placeholder='Last Name'
                            ref={(input) => { this.lastNameType = input }}
                            returnKeyType='next'
                            onSubmitEditing={() => this.usernameType.focus()}
                        />
                        <TextInput
                            value={this.state.email}
                            onChangeText={(email => this.setState({ email }))}
                            style={[style.basicInput, style.textInputProfile]}
                            underlineColorAndroid={mainColor}
                            placeholder='Email'
                            editable={false}
                        />
                        <TextInput
                            value={this.state.username}
                            onChangeText={(username => this.setState({ username }))}
                            style={[style.basicInput, style.textInputProfile]}
                            underlineColorAndroid={mainColor}
                            placeholder='Username'
                            ref={(input) => { this.usernameType = input }}
                        />
                        <TouchableOpacity onPress={() => this.setState({ datePicker: !this.state.datePicker })}>
                            <TextInput
                                value={this.state.birth}
                                style={[style.basicInput, style.textInputProfile]}
                                underlineColorAndroid={mainColor}
                                placeholder='Date of Birth'
                                editable={false}
                            />
                        </TouchableOpacity>
                        {this.state.datePicker &&
                            <DateTimePicker
                                value={this.state.birth == '' ? 315558573000 : Date.parse(this.state.birth)}
                                mode='date'
                                display='default'
                                onChange={this.setDate}
                                maximumDate={1136013109000}
                            />
                        }
                    </ScrollView>
                    <View style={[style.pressButton, style.pressButtonAlt, style.elevate, { marginTop: 20 }]}>
                        <TouchableOpacity onPress={this.submitButton}>
                            <Text style={[style.pressButtonTxt, style.pressButtonTxtAlt]}>
                                Edit Profile
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        uploader: state.uploader
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userUpdate: (firstName, lastName, username, birth) => {
            dispatch(userUpdate(firstName, lastName, username, birth))
        },
        uploadDisplayPicture: (source) => {
            dispatch(uploadDisplayPicture(source))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserEditProfile)