import React, { Component } from 'react'
import { ActivityIndicator, TouchableOpacity, Text, View, TextInput, Alert } from 'react-native'
import style from '../styles'
import { connect } from 'react-redux'
import { userLogin } from '../action/auth'
import { mainColor, secColor, thirdColor } from '../styles/colors'
import { email, password } from '../validators'
import AsyncStorage from '@react-native-community/async-storage'

class Login extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            emailChange: false,
            passwordChange: false,
        }
    }

    validEmail = () => {
        return this.state.emailChange ? email.test(this.state.email) : !this.state.emailChange
    }

    validPass = () => {
        return this.state.passwordChange ? password.test(this.state.password) : !this.state.passwordChange
    }

    buttonDisable = () => {
        if (
            email.test(this.state.email) &&
            password.test(this.state.password)
        ) {
            return false
        } else {
            return true
        }
    }

    loginButton = () => {
        this.props.userLogin(this.state.email, this.state.password)
        this.setState({
            buttonPressed: true
        })
    }

    render() {
        if (this.props.user.loading) {
            return (
                <View style={[style.fullContainer, style.container]}>
                    <ActivityIndicator color={secColor} size='large' />
                </View>
            )

        } else {
            if (this.state.buttonPressed) {
                if (this.props.user.auth) {
                    AsyncStorage.setItem('USER', JSON.stringify(this.props.user.payload[0].user))
                        .then(() => this.props.navigation.navigate('Feed'))

                } else {
                    Alert.alert('Failed', this.props.user.payload)
                    this.setState({
                        buttonPressed: false
                    })
                }
            }
        }
        return (
            <View style={[style.fullContainer, style.container]}>
                <Text style={style.titleHeader}>
                    Login
                </Text>
                <View style={style.formContainer}>
                    <TextInput
                        style={style.basicInput}
                        placeholder='Email'
                        underlineColorAndroid={secColor}
                        value={this.state.email}
                        onChangeText={email => this.setState({ email })}
                        keyboardType='email-address'
                        onBlur={() => {
                            this.setState({ emailChange: true })
                        }}
                        onSubmitEditing={() => {
                            this.passwordType.focus();
                        }}
                        returnKeyType='next'
                    />
                    {this.validEmail() ? null :
                        <Text style={style.bottomWarning}>
                            *Only using Real University email
                        </Text>}
                    <TextInput
                        style={style.basicInput}
                        placeholder='Password'
                        underlineColorAndroid={secColor}
                        value={this.state.password}
                        onChangeText={password => this.setState({ password })}
                        ref={(input) => { this.passwordType = input; }}
                        onBlur={() => {
                            this.setState({ passwordChange: true })
                        }}
                        returnKeyType='done'
                        secureTextEntry={true}
                    />
                    {this.validPass() ? null :
                        <Text style={style.bottomWarning}>
                            *Password must be have at least 6 character
                        </Text>}
                    <View style={style.buttonMargin} />
                    <View style={this.buttonDisable() ? [style.pressButton, style.disabled] : [style.pressButton]}>
                        <TouchableOpacity disabled={this.buttonDisable()} onPress={this.loginButton}>
                            <Text style={style.pressButtonTxt}>
                                Login
                            </Text>

                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userLogin: (email, password) => {
            dispatch(userLogin(email, password))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)