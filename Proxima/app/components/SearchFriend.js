import React, { Component } from 'react'
import { Image, ActivityIndicator, Text, View, TouchableOpacity, TextInput, FlatList } from 'react-native'
import style from '../styles'
import { connect } from 'react-redux'
import { findUser } from '../action/user'
import { mainColor, secColor, thirdColor, forthColor } from '../styles/colors'
import AsyncStorage from '@react-native-community/async-storage'
import { NavigationEvents } from 'react-navigation';
import { server } from '../config'

class FindUser extends Component {
    static navigationOptions = {
        header: null
    }

    state = {
        contain: '',
        userEmail: ''
    }

    async componentDidMount() {
        const userEmail = JSON.parse(await AsyncStorage.getItem('USER')).email
        this.setState({
            userEmail: userEmail
        })
    }

    friendSugestion = (text) => {
        this.setState({ contain: text })
    }

    friendProfile = (user) => {
        this.props.navigation.navigate('Profile', user)
    }

    renderFind = (find) => {
        if (find.email == this.state.userEmail || this.state.contain == '') {
            return null
        }
        if ((find.firstName).includes(this.state.contain) || (find.lastName).includes(this.state.contain)) {
            return (
                <TouchableOpacity onPress={() => this.friendProfile(find)}>
                    <View style={[style.findContainer, style.elevate]}>
                        <Image source={
                            find.profilePic ?
                                { uri: `${server}/image/profileImg/${find.profilePic}` } :
                                require('../src/img/default-profile-picture.jpg')
                        } style={[style.findDP]}
                        />
                        <View style={{justifyContent: 'center', marginLeft: 10}}>
                            <View style={style.mergeFirstLastName}>
                                <Text style={style.nameStyle}>{find.firstName} </Text>
                                <Text style={style.nameStyle}>{find.lastName}</Text>
                            </View>
                            <Text style={style.usernameStyle}>{find.username}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )
        }
        return null
    }

    render() {
        return (
            <View style={[style.fullContainer, style.container]}>
                <NavigationEvents
                    onWillFocus={() => this.props.findUser()}
                />
                <View style={style.secContainer}>
                    <TextInput
                        style={style.captionInput}
                        placeholder='Type your friend name here'
                        onChangeText={this.friendSugestion}
                    />
                    {this.props.find.loading ?
                        <ActivityIndicator color={mainColor} size='large' /> :
                        (this.props.find.result == null ?
                            <Text>
                                Find Friend
                    </Text> :
                            <FlatList
                                keyExtractor={(item, index) => index.toString()}
                                data={(this.props.find.result)}
                                renderItem={(find) => this.renderFind(find.item)}
                            />)
                    }
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        find: state.find
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        findUser: () => {
            dispatch(findUser())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FindUser)