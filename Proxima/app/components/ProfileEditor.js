import React, { Component } from 'react'
import { ToastAndroid, Alert, ScrollView, ActivityIndicator, Image, Text, View, TextInput, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import style from '../styles'
import { mainColor, secColor, thirdColor } from '../styles/colors'
import { userUpdate } from '../action/auth'
import { uploadDisplayPicture } from '../action/user'
import DateTimePicker from '@react-native-community/datetimepicker'
import ImagePicker from 'react-native-image-picker'
import { name, username } from '../validators'
import AsyncStorage from '@react-native-community/async-storage'

class ProfileEditor extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            firstName: '',
            lastName: '',
            username: '',
            birth: '',
            password: '',
            datePicker: false,
            buttonPressed: false
        }
    }

    async componentDidMount() {
        try {
            const value = await AsyncStorage.getItem('USER')
            const valueJson = JSON.parse(value)
            if (value !== null) {
                this.setState({
                    email: valueJson["email"],
                    password: valueJson["password"]
                })
            }
        } catch (err) {
            console.log(err)
        }
        const user = this.props.user.payload
        try {
            if (user) {
                await AsyncStorage.setItem('USER', JSON.stringify(user))
                await AsyncStorage.setItem('LastPlace', 'ProfileEditor')
                this.setState({
                    email: user.email,
                    password: user.password
                })
            }
        } catch (err) {
            console.log(err)
        }

    }

    imagePicker = () => {
        const camOptions = {
            title: 'Change Picture',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(camOptions, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                const source = { uri: response.uri };
                this.props.uploadDisplayPicture(source, this.state.email, this.state.password)
                this.setState({
                    avatarSource: source,
                });
            }
        });
    }

    setDate = (event, date) => {
        date = date || this.state.date
        this.setState({
            datePicker: false,
            birth: date.toISOString().slice(0, 10)
        })
    }

    validFirstName = () => {
        return this.state.firstNameChange ? name.test(this.state.firstName) : !this.state.firstNameChange
    }

    validLastName = () => {
        return this.state.lastNameChange ? name.test(this.state.lastName) : !this.state.lastNameChange
    }

    validUsername = () => {
        return this.state.usernameChange ? username.test(this.state.username) : !this.state.usernameChange
    }

    validBirth = () => {
        return this.state.birth !== ''
    }

    submitButton = () => {
        const { firstName, lastName, birth, username } = this.state
        this.props.userUpdate(firstName, lastName, username, birth)
        this.setState({
            buttonPressed: true
        })
    }

    buttonDisable = () => {
        if (
            name.test(this.state.firstName) &&
            name.test(this.state.lastName) &&
            username.test(this.state.username) &&
            this.state.birth !== ''
        ) {
            return false
        } else {
            return true
        }
    }

    render() {
        if (this.props.user.loading) {
            return (
                <View style={[style.fullContainer, style.container]}>
                    <ActivityIndicator color={secColor} size='large' />
                </View>
            )

        } else {
            if (this.state.buttonPressed) {
                if (this.props.user.auth) {
                    ToastAndroid.show('Create profile success, now your account can be fully use', ToastAndroid.SHORT)
                    AsyncStorage.removeItem('LastPlace')
                    AsyncStorage.removeItem('USER')
                    this.props.navigation.navigate('Login')
                } else {
                    Alert.alert('Failed', this.props.user.payload)
                    this.setState({
                        buttonPressed: false
                    })
                }
            }
        }
        return (
            <View style={[style.fullContainer, style.container]}>
                {this.state.datePicker &&
                    <DateTimePicker
                        value={this.state.birth == '' ? 315558573000 : Date.parse(this.state.birth)}
                        mode='date'
                        display='default'
                        onChange={this.setDate}
                        maximumDate={1136013109000}
                    />
                }

                <Text style={[style.titleHeader]}>
                    Create Profile
                </Text>
                <TouchableOpacity onPress={this.imagePicker}>
                    <Image source={this.state.avatarSource ? this.state.avatarSource : require('../src/img/default-profile-picture.jpg')} style={style.displayPicture} />
                </TouchableOpacity>
                <View style={style.formContainer}>
                    <ScrollView >
                        <TextInput
                            style={style.basicInput}
                            underlineColorAndroid={secColor}
                            value={this.state.email}
                            editable={false}
                        />
                        <TextInput
                            style={style.basicInput}
                            underlineColorAndroid={secColor}
                            placeholder='Enter your first name'
                            value={this.state.firstName}
                            onChangeText={(firstName) => this.setState({ firstName })}
                        />
                        {this.validFirstName() ? null :
                            <Text style={style.bottomWarning}>
                                *First letter must be caplitalize and not contain space
                        </Text>}
                        <TextInput
                            style={style.basicInput}
                            underlineColorAndroid={secColor}
                            placeholder='Enter your last name'
                            value={this.state.lastName}
                            onChangeText={(lastName) => this.setState({ lastName })}
                        />
                        {this.validLastName() ? null :
                            <Text style={style.bottomWarning}>
                                *First letter must be caplitalize and not contain space
                        </Text>}
                        <TextInput
                            style={style.basicInput}
                            underlineColorAndroid={secColor}
                            placeholder='Enter your username'
                            value={this.state.username}
                            onChangeText={(username) => this.setState({ username })}
                        />
                        {this.validUsername() ? null :
                            <Text style={style.bottomWarning}>
                                *Just using A-Z,a-z,0-9 and not space
                        </Text>}
                        <TouchableOpacity onPress={() => this.setState({ datePicker: !this.state.datePicker })}>
                            <TextInput
                                style={style.basicInput}
                                underlineColorAndroid={secColor}
                                placeholder='Enter your birth'
                                value={this.state.birth}
                                editable={false}
                            />
                        </TouchableOpacity>
                        {this.validBirth() ? null :
                            <Text style={style.bottomWarning}>
                                *Please insert your birth
                        </Text>}
                    </ScrollView>
                </View>
                <TouchableOpacity onPress={this.submitButton} disabled={this.buttonDisable()}>
                    <View style={this.buttonDisable() ? [style.pressButton, style.disabled] : [style.pressButton]}>
                        <Text style={style.pressButtonTxt}>
                            Update
                    </Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        uploader: state.uploader
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userUpdate: (firstName, lastName, username, birth) => {
            dispatch(userUpdate(firstName, lastName, username, birth))
        },
        uploadDisplayPicture: (source) => {
            dispatch(uploadDisplayPicture(source))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileEditor)