import React, { Component } from 'react'
import { ImageBackground, Text, View, TouchableOpacity } from 'react-native'
import style from '../styles'
import AsyncStorage from '@react-native-community/async-storage'

export default class Landing extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
    }

    async componentDidMount () {
        try {
            const value = await AsyncStorage.getItem('LastPlace')
            if(value !== null) {
              this.props.navigation.navigate(value)
            }
          } catch(e) {
            console.log(e)
          }
    }

    render() {
        return (
            <View style={[style.fullContainer, style.container]}>
                <ImageBackground source={require('../src/img/red-burst-background.jpg')} style={[style.fullContainer, style.container]}>
                    <Text style={[style.titleHeader]}>
                        Welcome
                </Text>
                    <Text style={[style.litleDesc]}>
                        We glad to see you join to this app. In this app you can share your picture, share your happiness, share your activity with privacy and safe. This app is only available for Real University
                </Text>
                    <View style={[style.footer]}>
                        <View style={[style.basicButton, style.landRegisterButton]}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
                                <Text style={[style.landRegisterTitle]}>
                                    Register
                        </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[style.basicButton, style.landLoginButton]}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                                <Text style={[style.landLoginTitle]}>
                                    Login
                        </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}