import { combineReducers } from 'redux'
import { userAuth } from './auth'
import { uploader } from './uploader'
import { feedGet } from './feed'
import { findUser } from './user'

const combinedReducers = combineReducers({
    user: userAuth,
    uploader: uploader,
    feed: feedGet,
    find: findUser
})

const rootReducers = (state, action) => {
    return combinedReducers(state, action)
}

export default rootReducers