const initialState = {
    loading: false,
    auth: false,
    payload: '',
    err:'',
    feed: []
}

const userAuth = (state = initialState, action) => {
    switch (action.type) {
        case 'AUTH_PROCESS':
            return {
                loading: true
            }
        case 'AUTH_SUCCESS':
            return {
                ...state,
                payload: action.payload,
                auth: true,
                loading: false
            }
        case 'AUTH_FAILED':
            return {
                ...state,
                err: action.payload,
                auth: false,
                loading: false
            }
        default:
            return state
    }
}

export { userAuth }