const initialState = {
    loading: false,
    result: null,
    error: null,
    focus: null,
    getFolFeed: null
}

const findUser = (state = initialState, action) => {
    switch (action.type) {
        case 'FIND_PROCESS':
            return {
                ...state,
                loading: true
            }
            case 'FOLLOW_SUCCESS':
                return {
                    ...state,
                    loading: false
                }
        case 'FIND_SUCCESS':
            return {
                ...state,
                result: action.payload,
                loading: false
            }
        case 'FIND_FAILED':
            return {
                ...state,
                error: action.payload,
                loading: false
            }
        case 'GFF_SUCCESS':
            return {
                ...state,
                getFolFeed: action.payload,
                loading: false
            }
        case 'SET_FOCUS':
            return {
                ...state,
                focus: action.payload
            }
        case 'RESET_FOCUS':
            return {
                ...state,
                focus: null
            }
        default:
            return state
    }
}

export { findUser }