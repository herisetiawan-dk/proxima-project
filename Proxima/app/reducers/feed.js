const initialState = {
    loading: false,
    auth: false,
    feed: [],
    err: null,
    image: '',
    likeStatus: false
}

const feedGet = (state = initialState, action) => {
    switch (action.type) {
        case 'FEED_PROCESS':
            return {
                loading: true
            }
        case 'FEED_SUCCESS':
            return {
                ...state,
                feed: action.payload,
                err: null,
                auth: true,
                loading: false
            }
        case 'FEED_FAILED':
            return {
                ...state,
                err: action.payload,
                auth: false,
                loading: false
            }
        case 'IMAGE_TAKE':
            return {
                ...state,
                image: action.payload
            }
        case 'IMAGE_DELETE':
            return {
                ...state,
                image: ''
            }
        default:
            return state
    }
}

export { feedGet }