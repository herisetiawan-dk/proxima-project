const initialState = {
    loading: false,
    payload: ''
}

const uploader = (state = initialState, action) => {
    switch (action.type) {
        case 'UPLOAD_PROCESS':
            return {
                loading: true
            }
        case 'UPLOAD_SUCCESS':
            return {
                ...state,
                payload: action.payload,
                loading: false
            }
        case 'UPLOAD_FAILED':
            return {
                ...state,
                payload: action.payload,
                loading: false
            }
        default:
            return state
    }
}

export { uploader }