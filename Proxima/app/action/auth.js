import axios from 'axios'
import path from 'path'
import FormData from 'form-data'
import { server } from '../config'
import AsyncStorage from '@react-native-community/async-storage'

const authProcess = () => {
    return {
        type: 'AUTH_PROCESS'
    }
}

const authSuccess = (data) => {
    return {
        type: 'AUTH_SUCCESS',
        payload: data
    }
}

const authFailed = (data) => {
    return {
        type: 'AUTH_FAILED',
        payload: data
    }
}

export const userRegister = (email, password) => {
    let url = `${server}/register`
    return dispatch => {
        dispatch(authProcess())
        axios
            .post(url, {
                email,
                password
            })
            .then(response => {
                AsyncStorage.setItem('USER', JSON.stringify(response.data))
                    .then(() => dispatch(authSuccess(response.data)))

            })
            .catch((err) => {
                dispatch(authFailed(err.response.data[0].message))
            })
    }
}

export const userUpdate = (firstName, lastName, username, birth) => {
    let url = `${server}/profile`
    return dispatch => {
        dispatch(authProcess())
        AsyncStorage
            .getItem('USER')
            .then(result => {
                return JSON.parse(result)
            })
            .then(result => {
                axios
                    .put(url, {
                        firstName,
                        lastName,
                        username,
                        birth
                    },
                        {
                            headers: {
                                email: result.email,
                                password: result.password,
                            }
                        })
                    .then(response => dispatch(authSuccess(response.data)))
                    .catch((err) => {
                        dispatch(authFailed(err.response.data[0].message))
                    })
            })
    }
}

export const userLogin = (email, password) => {
    let url = `${server}/login`
    return dispatch => {
        dispatch(authProcess())

        axios
            .post(url, {
                email,
                password
            })
            .then(response => dispatch(authSuccess(response.data)))
            .catch(err => dispatch(authFailed(err.response.data[0].message)))
    }
}