import axios from 'axios'
import path from 'path'
import FormData from 'form-data'
import { server } from '../config'
import AsyncStorage from '@react-native-community/async-storage'

const uploadProcess = () => {
    return {
        type: 'UPLOAD_PROCESS'
    }
}

const followSuccess = () => {
    return {
        type: 'FOLLOW_SUCCESS'
    }
}

const uploadSuccess = (data) => {
    return {
        type: 'UPLOAD_SUCCESS',
        payload: data
    }
}

const uploadFailed = (data) => {
    return {
        type: 'UPLOAD_FAILED',
        payload: data
    }
}

const gffSuccess = (data) => {
    return {
        type: 'GFF_SUCCESS',
        payload: data
    }
}

const findProccess = () => {
    return {
        type: 'FIND_PROCESS'
    }
}

const findSuccess = (data) => {
    return {
        type: 'FIND_SUCCESS',
        payload: data
    }
}

const findFailed = (data) => {
    return {
        type: 'FIND_FAILED',
        payload: data
    }
}

export const uploadDisplayPicture = (source) => {
    const { uri } = source
    let url = `${server}/profile/picture`
    let data = new FormData()
    data.append('profile_picture',
        {
            uri: uri,
            name: path.basename(uri),
            type: `image/${(path.extname(uri)).slice(1)}`
        }
    )
    return dispatch => {
        dispatch(uploadProcess())
        AsyncStorage
            .getItem('USER')
            .then(result => {
                return JSON.parse(result)
            })
            .then(result => {
                axios
                    .put(url, data, {
                        headers: {
                            email: result.email,
                            password: result.password
                        }
                    })
                    .then(response => dispatch(uploadSuccess(response.data)))
                    .catch((err) => {
                        dispatch(uploadFailed(err.response.data.message))
                    })
            })
    }
}

export const findUser = () => {
    return dispatch => {
        dispatch(findProccess())
        axios
            .get(`${server}/find`)
            .then(response => dispatch(findSuccess(response.data)))
            .catch(err => dispatch(findFailed(err)))
    }
}

export const getFeedFollow = (email) => {
    return dispatch => {
        dispatch(findProccess())
        axios
            .post(`${server}/getfollow`, { email: email })
            .then(response => dispatch(gffSuccess(response.data)))
            .catch(err => dispatch(findFailed(err)))
    }
}

export const addFollower = (email, password, target) => {
    return dispath => {
        dispath(findProccess())
        AsyncStorage
            .getItem('USER')
            .then(result => {
                return JSON.parse(result)
            })
            .then(result => {
        axios
            .post(`${server}/follow`, {
                email: result.email,
                password: result.password,
                target: target
            })
            .then(() => dispath(followSuccess()))
        })
    }
}

export const setFocus = (user) => {
    return {
        type: 'SET_FOCUS',
        payload: user
    }
}

export const resetFocus = () => {
    return {
        type: 'RESET_FOCUS'
    }
}