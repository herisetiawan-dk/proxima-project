import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Register from '../components/Register'
import Landing from '../components/Landing'
import Login from '../components/Login'
import ProfileEditor from '../components/ProfileEditor'
import UserPage from '../components/UserPage'

const rootStack = createStackNavigator(
    {
        Home: { screen: Landing },
        Login: { screen: Login },
        Register: { screen: Register },
        ProfileEditor: { screen: ProfileEditor },
        Feed: { screen: UserPage },
    },
    {
        initialRouteName: 'Home'
    }
)

const Navigator = createAppContainer(rootStack)

export default Navigator