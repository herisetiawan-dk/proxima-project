import React, { Component } from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Feed from '../components/Feed'
import NewFeed from '../components/NewFeed'
import ListUser from '../components/ListUser'

const feedNavigator = createStackNavigator(
    {
        Feed: { screen: Feed },
        NewFeed: { screen: NewFeed },
        ListUser: {screen: ListUser}
    },
    {
        initialRouteName: 'Feed'
    }
)

const feedRoutes = createAppContainer(feedNavigator)

export default feedRoutes