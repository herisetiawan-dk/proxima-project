import React, { Component } from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Find from '../components/SearchFriend'
import UserProfile from '../components/UserProfile'
import ListUser from '../components/ListUser'

const friendNavigator = createStackNavigator(
    {
        Find: { screen: Find },
        Profile: { screen: UserProfile },
        ListUser: { screen: ListUser }
    },
    {
        initialRouteName: 'Find'
    }
)

const friendRoutes = createAppContainer(friendNavigator)

export default friendRoutes