import React from 'react';
import { View } from "react-native";
import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import UserProfile from '../components/UserProfile'
import Icon from 'react-native-vector-icons/AntDesign';
import { mainColor, secColor, thirdColor, forthColor } from '../styles/colors'
import { connect } from 'react-redux'
import feedRoutes from './feedRoutes';
import friendRoutes from './findFriend';
import profileRoutes from './profileRoutes'

const bottomTab = createMaterialBottomTabNavigator(
    {
        Feed: {
            screen: feedRoutes,
            navigationOptions: {
                tabBarLabel: 'Feed',
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon style={[{ color: tintColor }]} size={25} name={'copy1'} />
                    </View>),

            }
        },
        SearchFriend: {
            screen: friendRoutes,
            navigationOptions: {
                activeColor: mainColor,
                inactiveColor: forthColor,
                barStyle: { backgroundColor: secColor },
                tabBarLabel: 'Find',
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon style={[{ color: tintColor }]} size={25} name={'search1'} />
                    </View>),
            }
        },
        Profile: {
            screen: profileRoutes,
            navigationOptions: {
                tabBarLabel: 'Profile',
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon style={[{ color: tintColor }]} size={25} name={'user'} />
                    </View>),
            }
        },
    },
    {
        initialRouteName: "Feed",
        activeColor: mainColor,
        inactiveColor: forthColor,
        barStyle: { backgroundColor: secColor },
    },
);
const Container = createAppContainer(bottomTab);

const mapStateToProps = (state) => {
    return {
        feed: state.feed
    }
}
export default connect(mapStateToProps, null)(Container);